package piano;

import java.awt.BorderLayout;

import javax.swing.JFrame;


@SuppressWarnings("serial")
public class PianoFrame extends JFrame {

	private final PianoPanel pianoPanel;



	public PianoFrame(Piano piano) {
		setTitle("Piano");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(800, 250);
		setMinimumSize(getSize());

		// Centers the window on the screen
		setLocationRelativeTo(null);

		pianoPanel = new PianoPanel(piano);
		add(pianoPanel, BorderLayout.CENTER);
	}

}
