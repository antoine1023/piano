package piano;



public class Octave {

	private final Key[] keys = new Key[Note.values().length];



	public Octave() {
		for (int i = 0; i < Note.values().length; i++) {
			Note note = Note.values()[i];
			Key key = new Key(note);
			keys[i] = key;
		}
	}



	@Override
	public String toString() {
		String s = "Octave {";
		for (Key key : keys) {
			s += key + ", ";
		}
		s += "}";
		return s;
	}



	public Key[] getKeys() {
		Key[] r = new Key[keys.length];
		System.arraycopy(keys, 0, r, 0, keys.length);
		return r;
	}

	/**
	 * 
	 * @param target
	 * @return The index or -1.
	 */
	public int getKeyIndex(Key target) {
		for (int i = 0; i < keys.length; i++) {
			if (keys[i] == target) {
				return i;
			}
		}
		return -1;
	}

	public boolean containsKey(Key key) {
		return getKeyIndex(key) != -1;
	}
}
