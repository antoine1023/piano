package piano;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;


@SuppressWarnings("serial")
public class PianoPanel extends JPanel {

	public static final int KEY_WIDTH = 30,
			WHITE_KEY_HEIGHT = 150,
			BLACK_KEY_HEIGHT = 100,
			KEY_MARGIN = 4,
			KEY_NAME_MARGIN = 2,
			KEY_CHARACTER_MARGIN_BOTTOM = 20,
			OCTAVE_WIDTH = (KEY_WIDTH + KEY_MARGIN) * 7,
			OCTAVE_FOCUS_BAR_HEIGHT = 2,
			OCTAVE_FOCUS_BAR_MARGIN_TOP = 10,
			OCTAVE_FOCUS_TEXT_MARGIN_TOP = 18;


	public static final Color WHITE_KEY_COLOR = Color.WHITE,
			BLACK_KEY_COLOR = Color.BLACK,

			BACKGROUND_COLOR = new Color(0x444466),

			C_KEY_COLOR = new Color(0xddddff),
			NOTE_NAME_COLOR = Color.GRAY,
			BLACK_KEY_DOWN_COLOR = new Color(0xa64b00),
			WHITE_KEY_DOWN_COLOR = new Color(0xffb273),
			WHITE_KEY_CHARACTER_COLOR = WHITE_KEY_DOWN_COLOR,
			BLACK_KEY_CHARACTER_COLOR = BLACK_KEY_DOWN_COLOR;


	/** The keymap for an AZERTY keyboard */
	public static final String AZERTY_KEYBOARD_KEYMAP =
			"qzsedftgyhuj" // first octave
			+ "kolpmù$*"; // second octave



	//////////////////////////////////////////////////////////////////////////



	public final Piano piano;

	private final KeyButton[] keyButtons;

	/**
	 * The current mouse pressed {@link Pressable} or <code>null</code>
	 */
	private Pressable mousePressedObject;

	private int keyboardFocusedOctave;



	//////////////////////////////////////////////////////////////////////////



	public PianoPanel(final Piano piano) {
		this.piano = piano;

		keyButtons = new KeyButton[piano.getKeyCount()];

		int i = 0;
		for (Octave octave : piano.getOctaves()) {
			for (Key key : octave.getKeys()) {
				KeyButton keyButton = new KeyButton(piano, key);
				keyButtons[i++] = keyButton;
			}
		}

		keyboardFocusedOctave = 1;


		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				mouseClick(e, false);
			}

			@Override
			public void mousePressed(MouseEvent e) {
				mouseClick(e, true);
			}
		});

		addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {}

			@Override
			public void keyReleased(KeyEvent e) {
				keyPressedOrReleased(e, false);
			}

			@Override
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_RIGHT:
					moveKeyboardOctaveFocus(1);
					break;

				case KeyEvent.VK_LEFT:
					moveKeyboardOctaveFocus(-1);
					break;

				case KeyEvent.VK_SPACE:
					piano.setSustainPedal(! piano.getSustainPedal());
					break;

				default:
					keyPressedOrReleased(e, true);
				}
			}
		});

		setFocusable(true);
		setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 13));
	}



	//////////////////////////////////////////////////////////////////////////



	private void moveKeyboardOctaveFocus(int movement) {
		piano.releaseAllKeys();

		keyboardFocusedOctave += movement;
		if (keyboardFocusedOctave < 0)
			keyboardFocusedOctave = 0;
		else if (keyboardFocusedOctave >= piano.getOctaveCount())
			keyboardFocusedOctave = piano.getOctaveCount() - 1;
		repaint();
	}


	/**
	 * @param c
	 * @return The {@link KeyButton} or <code>null</code>.
	 */
	private KeyButton getKeyButtonFromCharacter(char c) {
		c = Character.toLowerCase(c);

		int index = AZERTY_KEYBOARD_KEYMAP.indexOf(c);
		if (index == -1)
			return  null;

		int keyIndex = keyboardFocusedOctave * 12 + index;

		if (keyIndex < keyButtons.length)
			return keyButtons[keyIndex];
		return null;
	}


	/**
	 * 
	 * @param keyButtonIndex
	 * @return The character or '\0'
	 */
	private char getCharacterFromKeyButtonIndex(int keyButtonIndex) {
		int i = keyButtonIndex - keyboardFocusedOctave * 12;
		if (i >= 0 && i < AZERTY_KEYBOARD_KEYMAP.length())
			return AZERTY_KEYBOARD_KEYMAP.charAt(i);
		else
			return '\0';
	}


	/**
	 * 
	 * @param keyButton
	 * @return The character or '\0'
	 */
	private char getCharacterFromKeyButton(KeyButton keyButton) {
		int index = getKeyButtonIndex(keyButton);
		if (index == -1)
			return '\0';
		return getCharacterFromKeyButtonIndex(index);
	}


	/**
	 * 
	 * @param keyButton
	 * @return The index or -1 if not found.
	 */
	private int getKeyButtonIndex(KeyButton keyButton) {
		for (int i = 0; i < keyButtons.length; i++) {
			if (keyButtons[i] == keyButton)
				return i;
		}
		return -1;
	}


	private void keyPressedOrReleased(KeyEvent e, boolean pressed) {
		KeyButton keyButton = getKeyButtonFromCharacter(e.getKeyChar());
		if (keyButton != null) {
			if (pressed)
				keyButton.press();
			else
				keyButton.release();
			repaint();
		}
	}


	public static int getOctaveWidth() {
		return (KEY_MARGIN + KEY_WIDTH) * 7; // Because there are 7 white keys
	}

	public static int getOctaveOffsetX(Piano piano, Octave octave) {
		return piano.getOctaveIndex(octave) * PianoPanel.getOctaveWidth();
	}

	public int getOctaveOffsetX(Octave octave) {
		return getOctaveOffsetX(piano, octave);
	}


	/**
	 * 
	 * @param x
	 * @param y
	 * @return The {@link KeyButton} which contains the point (x, y) or
	 * <code>null</code>.
	 */
	public KeyButton getKeyButtonAt(int x, int y) {
		// black keys
		for (KeyButton keyButton : keyButtons)
			if (keyButton.key.isSharped() && keyButton.contains(x, y))
				return keyButton;

		// white keys
		for (KeyButton keyButton : keyButtons)
			if (! keyButton.key.isSharped() && keyButton.contains(x, y))
				return keyButton;

		return null;
	}



	private void mouseClick(MouseEvent event, boolean pressed) {
		mouseClickAt(event.getX(), event.getY(), pressed);
	}


	private void mouseClickAt(int x, int y, boolean pressed) {
		KeyButton keyButton = getKeyButtonAt(x, y);

		if (pressed)
			mouseDown(keyButton);
		else
			mouseUp(keyButton);

		repaint();
	}


	private void mouseDown(Pressable pressed) {
		if (mousePressedObject != null) {
			if (mousePressedObject != pressed)
				mousePressedObject.release();
		}
		mousePressedObject = pressed;
		if (pressed != null)
			pressed.press();
	}

	private void mouseUp(Pressable released) {
		if (mousePressedObject != null) {
			mousePressedObject.release();
		}
		if (released != null) {
			if (released != mousePressedObject)
				released.release();
		}
		mousePressedObject = null;
	}


	@Override
	public void paint(Graphics g) {

		// Clean up the panel
		g.setColor(BACKGROUND_COLOR);
		g.fillRect(0, 0, getWidth(), getHeight());

		// First draw white keys
		for (KeyButton keyButton : keyButtons)
			if (! keyButton.key.isSharped())
				paintKey(keyButton, g);

		// Then draw black keys
		for (KeyButton keyButton : keyButtons)
			if (keyButton.key.isSharped())
				paintKey(keyButton, g);

		paintFocusedOctave(g);
	}


	private void paintFocusedOctave(Graphics g) {

		g.setColor(WHITE_KEY_DOWN_COLOR);

		g.fillRect(keyboardFocusedOctave * OCTAVE_WIDTH,
				WHITE_KEY_HEIGHT + OCTAVE_FOCUS_BAR_MARGIN_TOP,
				OCTAVE_WIDTH,
				OCTAVE_FOCUS_BAR_HEIGHT);

		g.drawString("←  Keyboard  →",
				keyboardFocusedOctave * OCTAVE_WIDTH,
				WHITE_KEY_HEIGHT + OCTAVE_FOCUS_BAR_MARGIN_TOP
				+ OCTAVE_FOCUS_TEXT_MARGIN_TOP);

	}


	/**
	 * Draws the key.
	 */
	private void paintKey(KeyButton button,
			Graphics g) {

		final Key key = button.key;

		g.setColor(button.getColor());
		g.setFont(getFont());

		int x = button.getLocationX();

		int noteNameX = x + KEY_NAME_MARGIN;
		int noteNameY = button.getHeight() - KEY_NAME_MARGIN;

		int keyCharacterY = button.getHeight()
				- KEY_CHARACTER_MARGIN_BOTTOM;

		String noteName = key.note.frenchName;

		String character = String.valueOf(getCharacterFromKeyButton(button));

		if (key.isSharped()) {

			g.fillRect(x, 0, KEY_WIDTH, BLACK_KEY_HEIGHT);

			g.setColor(NOTE_NAME_COLOR);
			g.drawString(noteName, noteNameX, noteNameY);

			g.setColor(button.getKeyCharacterColor());
			g.drawString(character, noteNameX, keyCharacterY);
		} else {

			g.fillRect(x, 0, KEY_WIDTH, WHITE_KEY_HEIGHT);

			g.setColor(NOTE_NAME_COLOR);
			g.drawString(noteName, noteNameX, noteNameY);

			g.setColor(button.getKeyCharacterColor());
			g.drawString(character, noteNameX, keyCharacterY);
		}

	}

}
