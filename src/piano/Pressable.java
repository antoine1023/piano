package piano;


public interface Pressable {

	public void press();
	public void release();

}
