package piano;

import javax.sound.midi.MidiChannel;



public class Piano {

	private final Octave[] octaves;

	public final int firstOctaveNumber;

	private final MidiChannel midiChannel;

	private boolean sustainPedal;



	public Piano(int octaveCount, int firstOctaveNumber,
			MidiChannel midiChannel) {

		if (octaveCount < 1)
			throw new RuntimeException();

		octaves = new Octave[octaveCount];
		for (int i = 0; i < octaveCount; i++) {
			octaves[i] = new Octave();
		}

		this.firstOctaveNumber = firstOctaveNumber;
		this.midiChannel = midiChannel;
	}



	@Override
	public String toString() {
		String s = "Piano {\n";
		for (Octave octave : octaves) {
			s += "\t" + octave + ",\n";
		}
		s += "}\n";
		return s;
	}



	public Octave[] getOctaves() {
		Octave[] r = new Octave[octaves.length];
		System.arraycopy(octaves, 0, r, 0, octaves.length);
		return r;
	}

	public int getOctaveCount() {
		return octaves.length;
	}

	public int getOctaveIndex(Octave target) {
		for (int i = 0; i < octaves.length; i++) {
			if (octaves[i] == target) {
				return i;
			}
		}
		return -1;
	}

	public int getOctaveIndexFromKey(Key key) {
		Octave octave = getOctave(key);
		if (octave == null)
			throw new IllegalArgumentException();
		return getOctaveIndex(octave);
	}

	public int getOctaveNumber(Key key) {
		int octaveIndex = getOctaveIndexFromKey(key);
		if (octaveIndex == -1)
			throw new IllegalArgumentException();
		return firstOctaveNumber + octaveIndex;
	}



	/**
	 * 
	 * @param target
	 * @return The octave or <code>null</code>.
	 */
	public Octave getOctave(Key target) {
		for (Octave octave : octaves) {
			if (octave.containsKey(target))
				return octave;
		}
		return null;
	}



	public int getKeyCount() {
		return octaves.length * Note.values().length;
	}


	public int getMidiNoteName(Key key) {
		int octaveNumber = getOctaveNumber(key);
		return key.note.midiNoteName + 12 * octaveNumber;
	}


	public double getFrequency(Key key) {
		int octaveNumber = getOctaveNumber(key);
		double frequency = key.note.frequency * Math.pow(2.0, octaveNumber);
		return frequency;
	}


	public void pressKey(Key key) {
		midiChannel.noteOn(getMidiNoteName(key), 200);
		key.setPressed(true);
	}
	public void releaseKey(Key key) {
		if (! sustainPedal)
			midiChannel.noteOff(getMidiNoteName(key));
		key.setPressed(false);
	}

	public void releaseAllKeys() {
		for (Octave octave : octaves) {
			for (Key key : octave.getKeys()) {
				releaseKey(key);
			}
		}
	}



	public boolean getSustainPedal() {
		return sustainPedal;
	}

	public void setSustainPedal(boolean sustainPedal) {
		if (! sustainPedal && this.sustainPedal) {
			this.sustainPedal = sustainPedal;
			releaseAllKeys();
		} else {
			this.sustainPedal = sustainPedal;
		}
	}

}
