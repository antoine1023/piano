package piano;

import javax.sound.midi.Instrument;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;


public class Main {

	public static void main(String[] args) throws MidiUnavailableException, InterruptedException {

		Synthesizer synth = MidiSystem.getSynthesizer();
		synth.open();

		MidiChannel[] channels = synth.getChannels();
		MidiChannel channel = channels[0];

		Instrument[] instr = synth.getDefaultSoundbank().getInstruments();
		synth.loadInstrument(instr[0]);

		Piano piano = new Piano(3, -1, channel);

		PianoFrame frame = new PianoFrame(piano);
		frame.setVisible(true);
	}

}
