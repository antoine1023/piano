package piano;

import java.awt.Color;



public class KeyButton implements Pressable {

	public final Piano piano;
	public final Key key;



	public KeyButton(Piano piano, Key key) {
		this.piano = piano;
		this.key = key;
	}



	@Override
	public String toString() {
		return "KeyButton {" + key + "}";
	}



	public Octave getOctave() {
		return piano.getOctave(key);
	}


	private int getOctaveOffsetX() {
		return PianoPanel.getOctaveOffsetX(piano, getOctave());
	}


	public int getOffsetX() {
		int offsetX = 0;

		for (Key i : getOctave().getKeys()) {
			if (i == key)
				break;

			if (! i.isSharped()) {
				offsetX += PianoPanel.KEY_WIDTH;
				offsetX += PianoPanel.KEY_MARGIN;
			}
		}

		if (key.isSharped()) {
			offsetX -= (PianoPanel.KEY_WIDTH + PianoPanel.KEY_MARGIN) / 2;
		}

		return offsetX;
	}


	public int getLocationX() {
		return getOctaveOffsetX() + getOffsetX();
	}


	public int getHeight() {
		return key.isSharped() ?
				PianoPanel.BLACK_KEY_HEIGHT : PianoPanel.WHITE_KEY_HEIGHT;
	}



	public boolean contains(int x, int y) {
		int right = getLocationX() + PianoPanel.KEY_WIDTH;
		return x > getLocationX() && x < right
				&& y > 0 && y < getHeight();
	}


	public Color getColor() {
		if (key.isSharped()) {
			if (key.isPressed())
				return PianoPanel.BLACK_KEY_DOWN_COLOR;
			else
				return PianoPanel.BLACK_KEY_COLOR;
		} else {
			if (key.isPressed())
				return PianoPanel.WHITE_KEY_DOWN_COLOR;
			else
				if (key.note == Note.C)
					return PianoPanel.C_KEY_COLOR;
				else
					return PianoPanel.WHITE_KEY_COLOR;
		}
	}


	public Color getKeyCharacterColor() {
		if (key.isSharped()) {
			if (key.isPressed())
				return new Color(0xffffff);
			else
				return PianoPanel.BLACK_KEY_CHARACTER_COLOR;
		} else {
			if (key.isPressed())
				return new Color(0x000000);
			else
				return PianoPanel.WHITE_KEY_CHARACTER_COLOR;
		}
	}


	@Override
	public void press() {
		piano.pressKey(key);
	}

	@Override
	public void release() {
		piano.releaseKey(key);
	}

}
