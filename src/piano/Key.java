package piano;


public class Key {

	public final Note note;

	private boolean pressed;



	public Key(Note note) {
		if (note == null)
			throw new NullPointerException();

		this.note = note;
	}



	@Override
	public String toString() {
		return "Key {" + note + "}";
	}



	public boolean isSharped() {
		return note.sharped;
	}



	public boolean isPressed() {
		return pressed;
	}

	public void setPressed(boolean pressed) {
		this.pressed = pressed;
	}

}
