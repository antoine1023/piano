package piano;


public enum Note {

	/** C, Do */
	C(60, false, "C", "do", -9),

	/** C#, Do# */
	CS(61, true, "C#", "do#", -8),

	/** D, Ré */
	D(62, false, "D", "ré", -7),

	/** D# */
	DS(63, true, "D#", "ré#", -6),

	E(64, false, "E", "mi", -5),

	F(65, false, "F", "fa", -4),
	/** F# */
	FS(66, true, "F#", "fa#", -3),

	G(67, false, "G", "sol", -2),
	/** G# */
	GS(68, true, "G#", "sol#", -1),

	/** A, La */
	A(69, false, "A", "la", 0),

	/** A#, La# */
	AS(70, true, "A#", "la#", 1),

	/** B, Si */
	B(71, false, "B", "si", 2),
	;



	public final int midiNoteName;

	public final boolean sharped;

	public final String frenchName;
	public final String englishName;

	public final double frequency;



	private final static double A_BASE_FREQUENCY = 440.0;



	Note(int midiNoteName, boolean sharped, String englishName, String frenchName,
			int semitones) {

		this.midiNoteName = midiNoteName;
		this.sharped = sharped;
		this.frenchName = frenchName;
		this.englishName = englishName;

		final double semitoneFrequencyRatio = Math.pow(2.0, 1.0 / 12.0);

		frequency = A_BASE_FREQUENCY *
				Math.pow(semitoneFrequencyRatio, semitones);
	}

}
